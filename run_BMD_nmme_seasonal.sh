#!/bin/bash
#
# Automatically runs the BMD NMME-based seasonal forecssts
# Written by Dr Simon J. Mason
# Date: 15 April 2019

#
# DEFINTIONS
# (Modify the following definitions as needed)

#
# Directories

export CPT_BIN_DIR=$HOME/bin/      # CPT home directory

cpt_in=cpt_in/                     # CPT data input subdirectory
cpt_out=cpt_out/                   # CPT data output subdirectory
nmme=nmme/                         # NMME data subdirectory
clim=clim/                         # climate data subdirectory

wdir=$HOME/BMD/                    # working directory
ddir=${wdir}${cpt_in}              # data directory

#
# Actions
download=1
forecast=0

#
# Data

ban_map_n=27.5                     # Bangladesh northern domain limit
ban_map_s=20.0                     # Bangladesh southern domain limit
ban_map_w=87.0                     # Bangladesh western domain limit
ban_map_e=93.5                     # Bangladesh eastern domain limit

lsn=3                              # length of season

yr1=1981                           # first year of predictors to download
cyr1=1981                          # first year of clmatological period
cyrn=2010                          # last year of clmatological period

nmme_map_n=35                      # NMME predictor northern domain limit
nmme_map_s=15                      # NMME predictor southern domain limit
nmme_map_w=80                      # NMME predictor western domain limit
nmme_map_e=100                     # NMME predictor eastern domain limit

nmmefn=nmme                        # NMME data root filename

models=("CFS2" "CMC3" "CMC4")      # NMME models (select from "CFS2" "CMC3" "CMC4" "CCSM4" "FLOR-A02" "FLOR-B01" "CESM1" "GMAO" )


cmoni="JFMAMJJASONDJFMAMJJASOND"
cmona=("jan" "feb" "mar" "apr" "may" "jun" "jul" "aug" "sep" "oct" "nov" "dec")

#
# EXECUTIONS
#

#
# Identify the current year and month

syr=$(date +%Y)
smn=$(date +%m)

#
# Identify the previous month and the target month

case $smn in
01 )
   tmn='02' ;;
02 )
   tmn='03' ;;
03 )
   tmn='04' ;;
04 )
   tmn='05' ;;
05 )
   tmn='06' ;;
06 )
   tmn='07' ;;
07 )
   tmn='08' ;;
08 )
   tmn='09' ;;
09 )
   tmn='10' ;;
10 )
   tmn='11' ;;
11 )
   tmn='12' ;;
12 )
   tmn='01' ;;
esac

if [ $tmn = '01' ]
then
   tyr=`expr $syr + 1`
else
   tyr=$syr
fi

#
# Create results directory and enable read and execute access to other users

odir=${wdir}${cpt_out}${syr}${smn}/

if [ ! -d $odir ]
then
   mkdir $odir
   chmod 755 $odir
fi

#
# Repeat for each season

for issn in 1 $lsn
do

i=$(($tmn - 1))
if [ ${issn} = 1 ]
then
cssn=${cmona[$i]}
else
cssn=${cmoni:$i:$issn}
fi

#
# Download latest data

# Repeat for each model
   for cmodel in "${models[@]}"
   do

case "$cmodel" in
CFS2 )
   imodel=11 ;;
CMC3 )
   imodel=31 ;;
CMC4 )
   imodel=32 ;;
CCSM4 )
   imodel=41 ;;
FLOR-A02 )
   imodel=52 ;;
FLOR-B01 )
   imodel=53 ;;
CESM1 )
   imodel=61 ;;
GMAO )
   imodel=71 ;;
esac

#
# Repeat for each predictand

      for cvar in prcp wetdays tmax tmin
      do

case $cvar in
prcp )
   cvar_in=prcp
   ivar=61 ;;
wetdays )
   cvar_in=prcp
   ivar=61 ;;
tmax )
   cvar_in=tmax
   ivar=15 ;;
tmin )
   cvar_in=tmin
   ivar=16 ;;
esac

#
# Download NMME models
if [ ${download} = "1" ]
then
CPT.x << in1
572                                           # suppress menus
571                                           # Error handling
3                                             #    force CPT to stop
811                                           # Download
3                                             #    observations
$ivar                                         #    precipitation
$imodel                                       #    ERSSTs
$yr1                                          #    first year to download
$syr                                          #    last year to download
$tmn                                          #    first month of target season to download
$issn                                         #    length of season
$smn                                          #    month forecasts were initialized
${ddir}${nmme}${cmodel}_${cvar_in}_${smn}_${cssn}.tsv
0                                             # Exit
in1
fi

#
# Run CPT
if [ ${forecast} = "1" ]
then

CPT.x << in2
572                                           # suppress menus
571                                           # Error handling
3                                             #    force CPT to stop
611                                           # CCA
1                                             # Open X file
${ddir}${nmme}${cmodel}_${cvar_in}_${smn}_${cssn}.tsv
$nmme_map_n                                   #    northernmost latitude
$nmme_map_s                                   #    southernmost latitude
$nmme_map_w                                   #    westernmost longitude
$nmme_map_e                                   #    easternmost longitude
1                                             #    minimum number of X modes
8                                             #    maximum number of X modes
2                                             # Open Y file
${ddir}${clim}CRU_${cvar}.tsv
$ban_map_n                                    #    northernmost latitude
$ban_map_s                                    #    southernmost latitude
$ban_map_w                                    #    westernmost longitude
$ban_map_e                                    #    easternmost longitude
1                                             #    minimum number of Y modes
8                                             #    maximum number of Y modes
1                                             #    minimum number of CCA modes
8                                             #    maximum number of CCA modes
6                                             # set forecast period:
$syr                                          #    year from which to forecast
9                                             # set number of forecasts:
1                                             #    forecast one year only
532                                           # set climatological period
$cyr1                                         #    first year
$cyrn                                         #    last year
n                                             #    climatological period cannot extend beyond training period
2                                             #    seasonal totals
542                                           # switch on zero-bound
541                                           # switch on Y transformation
554                                           # select transformation
2                                             #    gamma distribution
131                                           # results files formats
2                                             #    GrADS
112                                           # save goodness index
${odir}${cmodel}_goodness_${cvar}_${cssn}
311                                           # cross-validate
455                                           # calculate forecasts
111                                           # output results
511                                           #    deterministic forecasts
${odir}${cmodel}_forecasts_${cvar}_${cssn}
501                                           #    tercile probabilities
${odir}${cmodel}_tercileprobs_${cvar}_${cssn}
0                                             # end output
413                                           # output skill maps
2                                             #    Spearman's correlations
${odir}${cmodel}_spearman_${cvar}_${cssn}
413                                           # output skill maps
10                                            #    ROC areas (below-normal)
${odir}${cmodel}_roc_b_${cvar}_${cssn}
413                                           # output skill maps
11                                            #    ROC areas (above-normal)
${odir}${cmodel}_roc_a_${cvar}_${cssn}
0                                             # quit CPT
in2
fi

# end cvar loop
      done

# end nmme model loop
   done

# end season length loop
done
