#!/bin/bash
#
# Automatically runs the BMD SST-based seasonal forecssts
# Written by Dr Simon J. Mason
# Date: 15 April 2019

#
# DEFINTIONS
# (Modify the following definitions as needed)

#
# Directories

export CPT_BIN_DIR=$HOME/bin/      # CPT home directory

cpt_in=cpt_in/                     # CPT data input subdirectory
cpt_out=cpt_out/                   # CPT data output subdirectory
sst_dir=sst/                       # SST data subdirectory
clim_dir=clim/                     # climate data subdirectory

wdir=$HOME/BMD/                    # working directory
ddir=$wdir$cpt_in                  # data directory

#
# Actions

download=0
forecast=1

#
# Data

tssn=("JJASO" "NDJF" "MAM")

ban_map_n=27.5                     # Bangladesh northern domain limit
ban_map_s=20.0                     # Bangladesh southern domain limit
ban_map_w=87.0                     # Bangladesh western domain limit
ban_map_e=93.5                     # Bangladesh eastern domain limit

yr1=1981                           # first year of predictors to download
cyr1=1981                          # first year of clmatological period
cyrn=2010                          # last year of clmatological period

sst_map_n=30                       # SST predictor northern domain limit
sst_map_s=-30                      # SST predictor southern domain limit
sst_map_w=30                       # SST predictor western domain limit
sst_map_e=270                      # SST predictor eastern domain limit

sstfn=ersst                        # SST data root filename

cmoni="JFMAMJJASONDJFMAMJJASOND"
cmona=("jan" "feb" "mar" "apr" "may" "jun" "jul" "aug" "sep" "oct" "nov" "dec")

#
# EXECUTIONS
#

#
# Identify the beginning and length of the seasons
j=0
for isn in "${tssn[@]}"
do
   lssn[j]=${#isn}
   k=0
   found=0
   until [ ${found} = "1" ]
   do
      cssn=${cmoni:$k:${lssn[j]}}
      if [ "${isn}" = "${cssn}" ]
      then
         sm1[j]=$((${k} + 1))
         found=1
      fi
      k=$(($k + 1))
   done
   j=$(($j + 1))
done
 
#
# Identify the current year and month

syr=$(date +%Y)
smn=$(date +%m)

#
# Identify the current season
nsn=${#tssn[@]}
found=0
isn=0
until [ ${found} = "1" ]
do
   m1=${sm1[isn]}
   m2=$((${m1} + ${lssn[isn]} - 1))
   if [ ${m2} -le 12 ] # current month is within the year
   then
      if [ ${smn} -ge ${m1} ] && [ ${smn} -le ${m2} ]
      then
         found=1
      fi
   else # season spans year-end
      m2=$(($m2 - 12))
      if [ ${smn} -ge ${m1} ] || [ ${smn} -le ${m2} ]
      then
         found=1
      fi
   fi
   isn=$(($isn + 1))
done
inext=${isn}
if [ ${inext} = ${nsn} ]
then
   inext=0
fi

cssn=${tssn[inext]}
tmn=${sm1[inext]}
lsn=${lssn[inext]}

#
# Identify the previous month

case $smn in
01 )
   pmn='12' ;;
02 )
   pmn='01' ;;
03 )
   pmn='02' ;;
04 )
   pmn='03' ;;
05 )
   pmn='04' ;;
06 )
   pmn='05' ;;
07 )
   pmn='06' ;;
08 )
   pmn='07' ;;
09 )
   pmn='08' ;;
10 )
   pmn='09' ;;
11 )
   pmn='10' ;;
12 )
   pmn='11' ;;
esac

if [ $pmn = '12' ]
then
   pyr=`expr $syr - 1`
else
   pyr=$syr
fi

if [ $tmn = '01' ]
then
   tyr=`expr $syr + 1`
else
   tyr=$syr
fi

#
# Download latest data

# SSTs
if [ ${download} = "1" ]
then
CPT.x << in1
572                              # Suppress menus
571                              # Error handling
3                                #    force CPT to stop
811                              # Download
1                                #    observations
80                               #    SSTs
1                                #    ERSSTs
$yr1                             #    first year to download
$pyr                             #    last year to download
$pmn                             #    month to download
1                                #    length of season
1                                #    number of seasons
${ddir}${sst_dir}${sstfn}_${pmn}.tsv
0                                # Exit
in1
fi

#
# Create results directory and enable read and execute access to other users

odir=${wdir}${cpt_out}${syr}${smn}/

if [ ! -d $odir ]
then
   mkdir $odir
   chmod 755 $odir
fi

#
# Repeat for each predictand

for cvar in prcp wetdays
do

#
# Run CPT
   if [ ${forecast} = "1" ]
   then
echo Forecasting $cvar for $cssn

CPT.x << in2
572                                 # Suppress menus
571                                 # Error handling
3                                   #    force CPT to stop
611                                 # CCA
1                                   # Open X file
${ddir}${sst_dir}/${sstfn}_${pmn}.tsv
$sst_map_n                          #    northernmost latitude
$sst_map_s                          #    southernmost latitude
$sst_map_w                          #    westernmost longitude
$sst_map_e                          #    easternmost longitude
1                                   #    minimum number of X modes
8                                   #    maximum number of X modes
2                                   # Open Y file
${ddir}${clim_dir}CRU_${cvar}.tsv
$tmn                                #    first month of season
$lsn                                #    length of season
$lsn                                #    SPI period (set as lsn to avoid persistence component)
$ban_map_n                          #    northernmost latitude
$ban_map_s                          #    southernmost latitude
$ban_map_w                          #    westernmost longitude
$ban_map_e                          #    easternmost longitude
1                                   #    minimum number of Y modes
8                                   #    maximum number of Y modes
1                                   #    minimum number of CCA modes
8                                   #    maximum number of CCA modes
6                                   # set forecast period:
$pyr                                #    year from which to forecast
9                                   # set number of forecasts:
1                                   #    forecast one year only
532                                 # set climatological period
$cyr1                               #    first year
$cyrn                               #    last year
n                                   #    climatological period cannot extend beyond training period
2                                   #    seasonal totals
542                                 # switch on zero-bound
541                                 # switch on Y transformation
554                                 # select transformation
2                                   #    gamma distribution
131                                 # results files formats
3                                   #    GrADS
112                                 # save goodness index
${odir}${sstfn}_goodness_${cvar}_${cssn}
311                                 # cross-validate
455                                 # calculate forecasts
111                                 # output results
25                                  #    deterministic forecasts
${odir}${sstfn}_forecasts_${cvar}_${cssn}
23                                  #    tercile probabilities
${odir}${sstfn}_tercileprobs_${cvar}_${cssn}
0                                   # end output
413                                 # output skill maps
2                                   #    Spearman's correlations
${odir}${sstfn}_spearman_${cvar}_${cssn}
413                                 # output skill maps
10                                  #    ROC areas (below-normal)
${odir}${sstfn}_roc_b_${cvar}_${cssn}
413                                 # output skill maps
11                                  #    ROC areas (above-normal)
${odir}${sstfn}_roc_a_${cvar}_${cssn}
0                                   # quit CPT
in2
   fi

done
